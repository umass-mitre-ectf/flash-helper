cd Arty-Z7-10 || exit 1
source /opt/pkg/petalinux/settings.sh
petalinux-build -x distclean
petalinux-build -v || exit 1
echo "Generating MES.BIN"
/opt/pkg/petalinux/tools/hsm/bin/bootgen -arch zynq -image SystemImage.bif -w -o MES.BIN
mv MES.BIN ..
cd ..
ls /dev/sdb1 > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "Mounting /dev/sdb1"
    sudo mount /dev/sdb1 /mnt || exit 1
    echo "Copying MES.BIN"
    sudo cp MES.BIN /mnt
    echo "Unmounting /dev/sdb1"
    sudo umount /dev/sdb1
    sync
    exit 0
fi
ls /dev/sdb > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "Mounting /dev/sdb"
    sudo mount /dev/sdb /mnt || exit 1
    echo "Copying MES.BIN"
    sudo cp MES.BIN /mnt
    echo "Unmounting /dev/sdb"
    sudo umount /dev/sdb
    sync
    exit 0
fi

