#include <tomcrypt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

typedef unsigned char uchar;

void compute_sha256(const uchar *in, const unsigned long len) {
	hash_state md;
	// We do not use this for now so exact sizing irrelevant
	unsigned char outarr[MAXBLOCKSIZE];

	sha256_init(&md);
	sha256_process(&md, in, len);
	sha256_done(&md, outarr);
}

int main(int argc, char ** argv) {
	if (argc==1 || argc>2) {
		puts("Usage: benchmark [iter_times]");
		return 2;
	}
	if (strcmp(argv[1],"-h")==0 || strcmp(argv[1],"--help")==0) {
		puts("Usage: benchmark [iter_times]");
	}
	
	unsigned long loop_times=strtoul(argv[1], NULL, 10);
	unsigned int proc_count = omp_get_num_procs();
	printf("Running benchmark %lu times with %u threads. A character will be printed every 1%% of the way.\n", loop_times, proc_count);
	
	struct timespec wcend, wcbegin;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &wcbegin);
	clock_t begin = clock();
	
	unsigned long percentfactor=loop_times/100;
	
#pragma omp parallel for
	for (unsigned long i=0;i<loop_times;i++) {
		// Guaranteed to be big enough due to size limits of unsigned long
		char num_str[32];
		sprintf(num_str,"%lu",i);
		compute_sha256((uchar*)num_str,strlen(num_str));
		if (i>=percentfactor) {
			#pragma omp critical
			{
				printf(".");
				percentfactor+=(loop_times/100);
				fflush(stdout);
			}
		}
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &wcend);
	clock_t end = clock();
	
	puts("\nDone."); // newline added automatically
	
	double wc_time_spent = (double)((wcend.tv_sec+wcend.tv_nsec*1e-9) - (double)(wcbegin.tv_sec+wcbegin.tv_nsec*1e-9));
	printf("Wall clock time spent is %f\n", wc_time_spent);
	
	double cpu_time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	printf("CPU clock time spent is %f\n", cpu_time_spent);
}
