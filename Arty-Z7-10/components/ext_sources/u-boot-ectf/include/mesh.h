#ifndef __MESH_H__
#define __MESH_H__

#define CONFIG_EXT4_WRITE
#include <ext4fs.h>

#define MAX_STR_LEN 64

// To erase (or call update) on flash, it needs to be done
// on boundaries of size 64K
#define FLASH_PAGE_SIZE 65536
// Memory page size for ARM architecture
#define MEM_PAGE_SIZE 4096

/*
    Helper functions
*/
int mesh_execute(char **args);
int mesh_num_builtins(void) ;
char* mesh_read_line(int bufsize);
int mesh_get_argv(char **args);
char **mesh_split_line(char *line) ;
char* mesh_input(char* prompt);
void ptr_to_string(void* ptr, char* buf);
int uboot_execute(const char * args[], size_t len);
int mesh_flash_write_command_internal(char **args, int increment);

/*
    Ext 4 functions
*/
int mesh_ls_ext4(const char *dirname, char *filename);
loff_t mesh_size_ext4(char *fname);
loff_t mesh_read_ext4(char *fname, char*buf, loff_t size);

/*
    Function Declarations for builtin shell commands:
 */
int mesh_help(char **args);
int mesh_shutdown(char **args);
int mesh_dump_flash(char **args);
int mesh_reset_flash(char **args);
int mesh_boot_kernel(char **args);
int mesh_n_flash_write_b(char **args);
int mesh_flash_write_b(char **args);
int mesh_flash_file_dump(char **args);
int mesh_memory_file_dump(char **args);
int mesh_memory_search(char **args);
void mesh_loop(void);

/*
 * Mesh flash commands
 */
int mesh_flash_init(void);
int mesh_flash_write(void* data, unsigned int flash_location, unsigned int flash_length);
int mesh_flash_read(void* data, unsigned int flash_location, unsigned int flash_length);

#endif
