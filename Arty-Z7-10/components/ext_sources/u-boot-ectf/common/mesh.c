#include <common.h>
#include <cli.h>
#include <stdlib.h>
#include <ext_common.h>
#include <ext4fs.h>
#include "../fs/ext4/ext4_common.h"
#include <fs.h>
#include <spi.h>
#include <spi_flash.h>
#include <command.h>
#include <os.h>

#include <mesh.h>
#include <boyer_moore.h>

#define MESH_TOK_BUFSIZE 64
#define MESH_TOK_DELIM " \t\r\n\a"
#define MESH_RL_BUFSIZE 1024
#define MESH_SHUTDOWN -2

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

/*
    List of builtin commands, followed by their corresponding functions.
 */
char *builtin_str[] = {
    "helpflashtool",
    "shutdown",
    "dumpflash",
    "resetflash",
    "bootkernel",
    "nflashwrite.b",
    "flashwrite.b",
    "flashdumpfile",
    "memorydumpfile",
    "memorysearch"
};

int (*builtin_func[]) (char **) = {
    &mesh_help,
    &mesh_shutdown,
    &mesh_dump_flash,
    &mesh_reset_flash,
    &mesh_boot_kernel,
    &mesh_n_flash_write_b,
    &mesh_flash_write_b,
    &mesh_flash_file_dump,
    &mesh_memory_file_dump,
    &mesh_memory_search
};


/******************************************************************************/
/********************************** Flash Commands ****************************/
/******************************************************************************/

/*
    This function initialized the flash memory for the Arty Z7. This must be done
    before executing any flash memory commands.
*/
int mesh_flash_init(void)
{
    char* probe_cmd[] = {"sf", "probe", "0", "2000000", "0"};
    return uboot_execute(probe_cmd, 5);
}

/*
    This is an improved version of the u-boot sf write. It allows you to update
    the flash not on the page bounderies. Normally, the flash write can only
    toggle 1's to 0's and erase can only reset the flash to 1's on page boundaries
    and in chunks of a single page.

    This is a wrapper that reads a page, updates the necessary bits, and then 
    updates the entire page in flash.

    It writes the byte array data of length flash_length to flash address at 
    flash_location.
*/

int mesh_flash_write(void* data, unsigned int flash_location, unsigned int flash_length)
{
    /* Write flash_length number of bytes starting at what's pointed to by data
     * to address flash_location in flash.
     */

    if (flash_length < 1)
        return 0;

    // We use the "sf update" command to update flash. Under the hood, this
    // calls out to "sf erase" and "sf write". The "sf erase" command needs
    // to be called on erase page boundaries (size 64 KB), so we need to make
    // sure that we only call "sf update" on those boundaries as well.
    // Since we want to write data to arbitrary locations in flash
    // (potentially across page boundaries) we need to break our data up so
    // that we can write to said boundaries.
    //
    // To do so, we read in the whole page that we're going to write to into RAM,
    // update (in RAM) the data, and then write the page back out.

    // Determine the starting and ending pages so that we know how many pages
    // we need to write to
    unsigned int starting_page = flash_location / FLASH_PAGE_SIZE;
    unsigned int ending_page = (flash_location + flash_length) / FLASH_PAGE_SIZE;

    // malloc space to hold an entire page
    char* flash_data = malloc(sizeof(char) * FLASH_PAGE_SIZE);

    // Find the sf sub command, defined by u-boot
    cmd_tbl_t* sf_tp = find_cmd("sf");

    // The number of bytes that we've copied to flash so far
    // This is so that we know when we've copied flash_length
    // number of bytes
    unsigned int bytes_copied = 0;

    // Loop over all of the pages that our data would touch and
    // write the modified pages
    for(unsigned int i = starting_page; i <= ending_page; ++i)
    {
        // Get the address (in flash) of the page we need to write
        unsigned int page_starting_address = i * FLASH_PAGE_SIZE;
        // read all of the page data into a buffer
        mesh_flash_read(flash_data, page_starting_address, FLASH_PAGE_SIZE);

        // If this is the first page, we need to stop on the page boundary
        // or once we've written the correct number of bytes specified by
        // flash_length
        if (i == starting_page)
        {
            // Copy (byte by byte) until we've either reached the end of
            // this page, or we've copied the appropriate number of bytes
            for (;
                 (flash_location + bytes_copied < page_starting_address + FLASH_PAGE_SIZE) && (bytes_copied < flash_length);
                 ++bytes_copied)
            {
                flash_data[(flash_location % FLASH_PAGE_SIZE) + bytes_copied] = ((char*) data)[bytes_copied];
            }
        }
        // Otherwise, we either have an entire page that needs to be updated,
        // or a partial page that we need to update. Either way, this page
        // starts on a page bound
        else
        {
            // Copy (byte by byte) until we've either reached the end of
            // this page, or we've copied the appropriate number of bytes
            for (unsigned int j = 0;
                 (i * FLASH_PAGE_SIZE + j < (i + 1) * FLASH_PAGE_SIZE) && (bytes_copied < flash_length);
                 ++j)
            {
                flash_data[j] = ((char*) data)[bytes_copied];
                ++bytes_copied;
            }
        }

        // We need to convert things to strings since this mimics the command prompt
        char data_ptr_str[11] = "";
        char offset_str[11] = "";
        char length_str[11] = "";

        // Convert the pointer to a string representation (0xffffffff)
        ptr_to_string(flash_data, data_ptr_str);
        ptr_to_string((void *) page_starting_address, offset_str);
        ptr_to_string((void *) FLASH_PAGE_SIZE, length_str);

        // Perform an update on this page
        char* write_cmd[] = {"sf", "update", data_ptr_str, offset_str, length_str};
        sf_tp->cmd(sf_tp, 0, 5, write_cmd);
    }

    free(flash_data);

    return 0;
}

/*
    This function reads flash_length bytes from the flash memory at flash_location
    to the byte array data.
*/
int mesh_flash_read(void* data, unsigned int flash_location, unsigned int flash_length)
{
    /* Read "flash_length" number of bytes from "flash_location" into "data" */

    // Find the sf sub command
    cmd_tbl_t* sf_tp = find_cmd("sf");

    // We need to convert things to strings since this mimics the command prompt,
    // so get us space for strings
    char str_ptr[11] = "";
    char offset_ptr[11] = "";
    char length_ptr[11] = "";
    // Convert the point to a string representation
    ptr_to_string(data, str_ptr);
    ptr_to_string((unsigned int *) flash_location, offset_ptr);
    ptr_to_string((unsigned int *) flash_length, length_ptr);

    // Perform an update
    char* read_cmd[] = {"sf", "read", str_ptr, offset_ptr, length_ptr};
    return sf_tp->cmd(sf_tp, 0, 5, read_cmd);
}

/******************************************************************************/
/******************************** End Flash Commands **************************/
/******************************************************************************/

/******************************************************************************/
/********************************** MESH Commands *****************************/
/******************************************************************************/

/* 
    This function lists all commands available from the mesh shell. It
    implements the help function in the mesh shell. 
*/
int mesh_help(char **args)
{
    /* List all valid commands */
    unsigned short i;
    printf("Welcome to the flash helper tool\n");
    printf("The extra commands available to you are listed below:\n");

    for (i = 0; i < mesh_num_builtins(); i++)
    {
        printf("  %s\n", builtin_str[i]);
    }
    printf("You may escape to the u-boot shell using %%.\n");
    printf("Use %%help for help with u-boot.\n");

    return 0;
}

/* 
    This shuts down the mesh terminal. It does not shut down the board.
    This implements the shutdown function in the mesh shell 
*/
int mesh_shutdown(char **args)
{
    return MESH_SHUTDOWN;
}

/* 
    This is a development utility that allows you to easily dump flash
    memory to std out.
*/
int mesh_dump_flash(char **args)
{
    int argv = mesh_get_argv(args);
    if (argv < 3){
        printf("Not enough arguments specified.\nUsage: dump offset size\n");
        return 0;
    }
    unsigned int size = simple_strtoul(args[2], NULL, 16);
    unsigned int offset = simple_strtoul(args[1], NULL, 16);
    printf("Dumping %u bytes of flash\n", size);
    char* flash = (char*) malloc(sizeof(char) * size);
    mesh_flash_read(flash, offset, size);

    // print hex in 16 byte blocks
    for(unsigned int i = 0; i < size; ++i)
    {
        if (i % 16 == 0)
        {
            printf("0x%06x ", i);
        }
        printf("%02x ", flash[i]);
        if (i % 16 == 15)
        {
        printf("\n");
        }
    }
    printf("\n");

    free(flash);

    return 0;
}

int mesh_reset_flash(char **args)
{
    // 0x1000000 is all 16 MB of flash
    // the erase page size is 64 KB or 0x10000 in hex
    char* probe_cmd[] = {"sf", "erase", "0", "0x1000000"};
    printf("Resetting flash. This may take more than a minute.\n");
    return uboot_execute(probe_cmd, 4);
}

int mesh_boot_kernel(char **args) {
    char * const boot_argv[2] = {"bootm", "0x10000000"};
    // Should not actually return
    return uboot_execute(boot_argv, 2);
}

/*
 * Simple command to write a value to flash.
 * Modeled after the interface of nm.b
 */
int mesh_n_flash_write_b(char **args) {
    return mesh_flash_write_command_internal(args, 0);
}

/*
 * Simple command to write a value to flash. Position incremented after each write.
 * Modeled after the interface of mm.b
 */
int mesh_flash_write_b(char **args) {
    return mesh_flash_write_command_internal(args, 1);
}

int mesh_flash_write_command_internal(char **args, int increment) {
    unsigned int ptrval;
    unsigned long ptrval_readin;
    short successflag = strict_strtoul(args[1], 16, &ptrval_readin);
    
    if (successflag!=0) {
        printf("Error: please enter a hexadecimal flash address\n");
        return 1;
    } else if (ptrval_readin>=0x1000000) {
        printf("Error: the requested address is too large\n");
        return 1;
    }
    // Guaranteed by this point to be within range
    ptrval=ptrval_readin;
    
    unsigned char bytedata;
    while(1) {
        mesh_flash_read(&bytedata, ptrval, 1);
        printf("%06x: %02x ? ", ptrval, bytedata);
        char * val_byte_ptr = mesh_read_line(5);
        unsigned long readinput;
        successflag = strict_strtoul(val_byte_ptr, 16, &readinput);
        if (successflag!=0 || readinput>=256 || (ptrval+increment)>=0x1000000) {
            break;
        }
        bytedata=readinput;
        mesh_flash_write(&bytedata, ptrval, 1);
        ptrval+=increment;
    }
    return 0;
}

/* 
    Dumps memory to files of size 0x100000 each.
    usage is command offset size, like dumpflash
    No arguments means dump all of memory
    Expected caveat: internal structures of this function will alter parts of memory in its execution
*/
int mesh_memory_file_dump(char **args)
{
    int argv = mesh_get_argv(args);
    unsigned int size = 0x20000000;
    unsigned int offset = 0;
    
    if (fs_set_blk_dev("mmc", "0:2", FS_TYPE_EXT) < 0) {
        printf("Error: unable to mount second partition\n");
        return -1;
    }
    if (argv >= 3){
        size = simple_strtoul(args[2], NULL, 16);
        offset = simple_strtoul(args[1], NULL, 16);
        if (offset>=0x20000000 || (offset+size)>0x20000000 || size==0) {
            printf("Invalid argument specified.\n");
        }
        printf("Dumping selected memory to files. This may take a while.\n");
    } else {
        printf("Dumping entire memory to files. This may take a while.\n");
    }
    
    // Dump 1 MiB at a time
    unsigned int malloc_size = size;
    if (malloc_size>0x100000) {
        malloc_size=0x100000;
    }
    unsigned char * buf = malloc(malloc_size);
    // Should be guaranteed but reminder to fix later
    assert(buf!=NULL);
    char * filename = malloc(9+11);
    // If malloc size is smaller, then loop only runs once
    for (unsigned int memloc = offset; memloc < offset+size; memloc += 0x100000) {
        unsigned char * memlocptr = (unsigned char *) memloc;
        printf("0x%p\n", memlocptr);
        snprintf(filename, 20, "/memdump_0x%p", memlocptr);
        // May pass through our own memory so account for overlap
        // Cast works because sizeof(void*) is 4 and sizeof(unsigned int) is also 4
        memmove(buf, memlocptr, malloc_size);

        loff_t actually_written;
        ext4_write_file(filename, buf, 0, malloc_size, &actually_written);

        if (actually_written != malloc_size) {
            printf("Warning: wrote %lld bytes instead of %u for location 0x%p\n", actually_written, MEM_PAGE_SIZE, memlocptr);
        }
    }

    free(buf);
    free(filename);
    ext4fs_close();
    printf("Done.\n");

    return 0;
}

int mesh_memory_search(char **args) {
    if (mesh_get_argv(args)<2) {
        printf("Please specify pattern to search: ELF, ECTF, or ZERO\n");
        return 1;
    }
    const unsigned char elf_pattern[4] = {'\x7f','E','L','F'};
    const unsigned char ectf_pattern[5] = {'e', 'c', 't', 'f', '{'};
    const unsigned char zero_pattern[4096] = {'\x00'};
    unsigned char * pattern;
    unsigned short pattern_len;
    if (strcmp(args[1], "ELF") == 0) {
        pattern = elf_pattern;
        pattern_len = 4;
    } else if (strcmp(args[1], "ECTF") == 0) {
        pattern = ectf_pattern;
        pattern_len = 5;
    } else if (strcmp(args[1], "ZERO") == 0) {
        pattern = zero_pattern;
        pattern_len = 4096;
    } else {
        printf("Valid patterns are ELF, ECTF, or ZERO\n");
        return 1;
    }
    // Function returns NULL if no result
    // There is a negligible chance that NULL actually has the pattern
    // This can be manually checked though
    unsigned char * pattern_ptr = 0;
    unsigned int mem_left = 0x20000000;
    printf("Searching memory...\n");
    while(1) {
        pattern_ptr = boyer_moore(pattern_ptr, mem_left, pattern, pattern_len);
        if (pattern_ptr==NULL) {
            break;
        }
        printf("0x%p\n",pattern_ptr);
        
        // Increment pointer past the found pattern
        pattern_ptr += pattern_len;
        mem_left = 0x20000000U - (unsigned int) pattern_ptr;
    }
    return 0;
}

/******************************************************************************/
/******************************** End MESH Commands ***************************/
/******************************************************************************/


/******************************************************************************/
/******************************** MESH Command Loop *****************************/
/******************************************************************************/

/*
    This is the main control loop for the mesh shell.
*/
void mesh_loop(void) {
    char *line;
    int status = 1;
    
    //printf("%%bdinfo\n");
    const char* bdinfo_cmd[] = {"bdinfo"};
    uboot_execute(bdinfo_cmd, 1);

    mesh_flash_init();
    
    printf("Run helpflashtool for info on custom commands.\n");
    printf("Escape to the regular u-boot shell by prefixing with %%.\n");

    while(1) {
        line = mesh_input(CONFIG_SYS_PROMPT);

        // This is the run_command function from common/cli.c:29
        // if this is uncommented, then it checks first in the builtins in
        // for the hush shell then for the command. This allows you to use
        // all the builtin commands when developing.
        if (line[0]=='%') {
            // Offset past percent symbol
            run_command(line+1, 0);
        } else {
            char **args;
            args = mesh_split_line(line);
            status = mesh_execute(args);
            free(args);
        }

        free(line);

        // -2 for exit
        if (status == MESH_SHUTDOWN)
            break;
    }
}

int mesh_flash_file_dump(char **args) {
    int retflag = 0;
    if(fs_set_blk_dev("mmc", "0:2", FS_TYPE_EXT) < 0){
        printf("Error: unable to mount second partition\n");
        return -1;
    }
    printf("Dumping flash to file. This may take a while\n");
    unsigned char * flashbuf = malloc(FLASH_PAGE_SIZE);
    mesh_flash_read(flashbuf, 0, FLASH_PAGE_SIZE);
    loff_t actually_written;
    ext4_write_file("/flashdump", flashbuf, 0, FLASH_PAGE_SIZE, &actually_written);
    if (actually_written != FLASH_PAGE_SIZE) {
        printf("Error: only %lld bytes written\n", actually_written);
        retflag = 1;
    }
    free(flashbuf);
    ext4fs_close();
    return retflag;
}

/******************************************************************************/
/****************************** End MESH Command Loop ***************************/
/******************************************************************************/

/******************************************************************************/
/*********************************** MESH Ext4 ********************************/
/******************************************************************************/

/* 
    This function gets the size of a file on a ext4 partion. It uses the
    u-boot ext4 fs functions to determine the size. 
*/
loff_t mesh_size_ext4(char *fname){
    loff_t size;    

    if(fs_set_blk_dev("mmc", "0:2", FS_TYPE_EXT) < 0){
        return -1;
    }

    // fs/fs.c:281
    ext4fs_size(fname, &size);

    ext4fs_close();

    return size;
}

loff_t mesh_read_ext4(char *fname, char*buf, loff_t size){
    loff_t actually_read;

    if(fs_set_blk_dev("mmc", "0:2", FS_TYPE_EXT) < 0){
        return -1;
    }

    ext4_read_file(fname, buf, 0, size, &actually_read);

    ext4fs_close();

    return actually_read;

}

/******************************************************************************/
/******************************* End MESH Ext4 ********************************/
/******************************************************************************/

/******************************************************************************/
/************************************* Helpers ********************************/
/******************************************************************************/

/*
    This function executes the specified command for the given user.
    It finds the command in builtin_func and then calls the function with the
    args for the given user.
*/
int mesh_execute(char **args) {
    unsigned short i;

    if (args[0] == NULL) {
        // An empty command was entered.
        return 1;
    }

    for (i = 0; i < mesh_num_builtins(); i++) {
        if (strcmp(args[0], builtin_str[i]) == 0) {
            return (*builtin_func[i])(args);
        }
    }

    printf("Not a valid command\n");
    printf("Use helpflashtool to get a list of valid commands\n");
    return 1;
}

/*
    This is a helper function to convert a character point to a hex string
    beginning with 0x. This is used for converting values to u-boot parameters
    which expects hex strings.
*/
void ptr_to_string(void* ptr, char* buf)
{
    /* Given a pointer and a buffer of length 11, returns a string of the poitner */
    sprintf(buf, "0x%x", (unsigned int) ptr);
    buf[10] = 0;
}

/*
    This function provides a wrapper for executing a command on the underlying u-boot shell.
    It also prints the executed command before executing it.
*/
int uboot_execute(const char * args[], size_t len) {
    printf("%%");
    for (size_t i=0;i<len;i++) {
        if (i!=0) {
            printf(" ");
        }
        printf("%s",args[i]);
    }
    printf("\n");
    cmd_tbl_t* tp = find_cmd(args[0]);
    return tp->cmd(tp,0,len,args);
}

/*
    This function determines the number of builtin functions in the mesh
    shell.
*/
int mesh_num_builtins(void) {
    return sizeof(builtin_str) / sizeof(char *);
}

/*
    This function reads a line from stdin and returns a pointer to the character
    buffer containing the null terminated line. 

    This funciton allocates the charater buffer on the heap, therefore, the caller
    must free this buffer to avoid a memory leak.
*/
char* mesh_read_line(int bufsize)
{
    int position = 0;
    char *buffer = (char*) malloc(sizeof(char) * bufsize);
    int c;

    while (1) {
        // Read a character
        c = getc();

        if (position == bufsize - 1) {
            printf("\b");
        }
        if (c == '\n' || c == '\r') {
            printf("\n");
            buffer[position] = '\0';
            return buffer;
        }
        else if (c == '\b' || c == 0x7F) // backspace
        {
            if (position)
            {
                position--;
                buffer[position] = '\0';
                printf("\b \b");
            }
        }
        else {
            buffer[position] = c;
            if (position < bufsize - 1)
            {
                position++;
            }
            printf("%c", c);
        }
    }
}

/* 
    This function determines the number of arguments specified in args and
    returns that number..
*/
int mesh_get_argv(char **args){
    int count = 0;

    for (unsigned short i=0; args[i]; i++){
        count++;
    }

    return count;
}

/*
    This function is used to split a single line of command line arguments
    into an array of individual arguments. 

    It returns an array of character buffers. Both this array and the character
    buffers are allocated on the heap and therefore, it is the responsibility of
    the caller to free this memory after the arguments are used.
*/
char **mesh_split_line(char *line) {
    int bufsize = MESH_TOK_BUFSIZE, position = 0;
    char **tokens = (char**) malloc(bufsize * sizeof(char*));
    char *token, **tokens_backup;

    token = strtok(line, MESH_TOK_DELIM);
    while (token != NULL) {
        tokens[position] = token;
        position++;

        if (position >= bufsize) {
            bufsize += MESH_TOK_BUFSIZE;
            tokens_backup = tokens;
            tokens = realloc(tokens, bufsize * sizeof(char*));
            if (!tokens) {
                free(tokens_backup);
            }
        }

        token = strtok(NULL, MESH_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}

/*
    This function prompts from user input from stdin and returns a point to
    that read line. Note, this is line is created using mesh_read_line and thus
    it is the responsibility of the caller to free the character buffer.
*/
char* mesh_input(char* prompt)
{
    printf(prompt);
    return mesh_read_line(MAX_STR_LEN);
}

